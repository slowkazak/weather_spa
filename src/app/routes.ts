import {provideRouter, RouterConfig} from '@angular/router';
import {AppMyweatherComponent} from "./component/app.myweather.component";
import {AppGreetingComponent} from "./component/app.greeting.component";
import {LoginPage} from "./component/app.register.component";


export const routes:RouterConfig = [
  {path: '', redirectTo:'/register',  terminal:true},
  {
    path: 'mysities',
    children: [
      {path:'', component: AppMyweatherComponent },
    ]
  },
  {
    path: 'greeting',
    component: AppGreetingComponent
  },
  {
    path: 'register',
    component: LoginPage
  }
];
export const APP_ROUTER_PROVIDERS = [provideRouter(routes)];
