export interface WeatherInterface {
  base: string,
  clouds: any,
  cod: number,
  coord: Object,
  dt: number,
  id: number,
  main: any,
  name: string,
  sys: any,
  weather: any,
  wind: Object
}
