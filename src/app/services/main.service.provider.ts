import {Injectable} from "@angular/core";
import {Jsonp, Response, Http, URLSearchParams} from '@angular/http';

import {Observable}     from 'rxjs/Observable';
import "../lib/lib";
import {settings} from "../settings"
@Injectable()
export class MainServiceProvider {
  private _getdatafirstinit:boolean = false;
  constructor(private jsonp:Jsonp, private http:Http) {
  }
//function for get pages uses _GET requests
  public getPage(id?:number) {
    return this.http.get(settings.samplepagepath)
      .map(request => this._extractData(request))
      .catch(this._handleError)
      .publishReplay(1)
      .refCount();
  }

  //main request to server function. need refactor. code is uqly
  public getCity(item?:string) {

   //skeleton for array of cities
    let citylist:any = [];
    // url for api requests
    let url:string = settings.apiurl + settings.apiver + '/' + settings.defparam;
    // skeleton for observables
    let observable:Observable<Object>;
    //
    let adressarray:Array<any> = [];
    let params:URLSearchParams = new URLSearchParams();
    params.set('appid', settings.apikey);
    params.set('callback', 'JSONP_CALLBACK');
    params.set('units', settings.units);
    params.set('lang', settings.lang);

//if function execute first-time until app is started
    if (!this._getdatafirstinit) {
      citylist = this._GetLocalstorage(settings.localstoragekey);


      //foreaching array of cities
      for (let city of citylist) {
        params.set('q', city);

//push in urls array jsonp requets
        adressarray.push(this.jsonp.get(url, {search: params})
          .map(request => this._extractData(request))
          .catch(this._handleError)
          .publishReplay(1)
          .refCount());
      }
      observable = Observable.forkJoin(adressarray);
    }
    //if function execute more then first
    else {
      citylist = this._GetLocalstorage(settings.localstoragekey);
      if (!citylist) {
        citylist = settings.defcity
      }
      //if usersearching term no exist
      if (!item) {
        //foreaching array of cities
        for (let city of citylist) {
          params.set('q', city);
//push in urls array jsonp requets
          adressarray.push(this.jsonp.get(url, {search: params})
            .map(request => this._extractData(request))
            .catch(this._handleError)
            .publishReplay(1)
            .refCount());
        }
        observable = Observable.forkJoin(adressarray);
      }
      else {
        //checking for exisisting searching city in array of cities
        let isexist:number = citylist.map(fld=>item).indexOf(citylist);
        if (isexist < 0) {
          citylist.push(item);
          this._SetLocalstorage(settings.localstoragekey, citylist);
          //если нет то мы будем делать только один запрос с этим городом
          params.set('q', item);
          observable = this.jsonp.get(url, {search: params})
            .map(request => this._extractData(request))
            .catch(this._handleError)
            .publishReplay(1)
            .refCount();
        }
      }
    }
    this._getdatafirstinit = true;
    //returning forkJOin or single jsonp request
    return observable;
  }

//remove data form localstorage
  public Remove_data(index:number) {
    let msg:string;
    if (settings.uselocalstoragetosave) {
      let localstorage:any = this._GetLocalstorage(settings.localstoragekey);
      localstorage.splice(index, 1);
      this._SetLocalstorage(settings.localstoragekey, localstorage.toString());
      msg = "success";
    }
    else msg = "err";
    return msg
  }
//setting localstorage
  private _SetLocalstorage(localstoragekey:string, data:Array<string>) {
    if (settings.uselocalstoragetosave) {
      let arraytostore = data.toString();
      localStorage.setItem(localstoragekey, arraytostore);
      return this._GetLocalstorage(settings.localstoragekey) || "error";
    }
  }
//getting localstorage
  private _GetLocalstorage(localstoragekey:string) {
    let localstorage:any = localStorage.getItem("citylist");
    if (settings.uselocalstoragetosave) {
      if (typeof(Storage) !== 'undefined') {
        if (localstorage) {
          localstorage = localstorage.split(",");
        }
        else {
          localStorage.setItem(localstoragekey, settings.defcity.toString());
        }
      }
      else {
        localstorage = settings.defcity;
        console.error(settings.localstoragesupporterrormsg);
      }
    }
    else {
      localstorage = settings.defcity;
      console.error(settings.localstoragesavingdisablemsg);
    }
    return localstorage || [];
  }

  //data handler function
  private _extractData(res:Response) {
    let body = res.json();
    return body || {};
  }

  //error handler function
  private _handleError(error:any) {

    let errMessage = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'server error';
    console.error(errMessage);
    return Observable.throw(errMessage);
  }
}
