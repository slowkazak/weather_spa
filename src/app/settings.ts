// global app settings need if no exist proxy for weather api. need remove
export const settings = {
  "apikey": "9d2f20fed5e9e290a1cd978802e0c707",
  "units": "metric",
  "lang": "ru",
  "uselocalstoragetosave": false,
  "apiver": "2.5",
  "defparam":"weather",
  "apiurl": "http://api.openweathermap.org/data/",
  "googleapikey": "AIzaSyAPNC0CMnE6JnojXlUNneM7iT0uo36AV70",
  "defcity": ['Moscow', 'London'],
  "localstoragesupporterrormsg": `LocalStorage не поддерживается или запрещено, используется список городов по умолчанию`,
  "localstoragesavingdisablemsg": `Запись в LocalStorage отключена, используется список городов по умолчанию`,
  "localstoragekey": 'citylist',
  "samplepagepath": "data/page.json"
}
