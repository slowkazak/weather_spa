import {Component, Output, EventEmitter} from "@angular/core";
@Component({
  moduleId: module.id,
  selector: 'city-search',
  templateUrl: '../app.searchcity.component.html'
})
export class AppSearchcityComponent {
  @Output()
  public searchcity = new EventEmitter();
  private _inputvalue: string;

  constructor() {
    this._inputvalue = '';
  }
}
