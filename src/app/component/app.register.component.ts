import {Component} from '@angular/core';
import {FormBuilder, Validators} from '@angular/common';
@Component({
  moduleId: module.id,
  selector: 'app-register',
  templateUrl: '../app.register.component.html'
})
export class LoginPage {
  loginForm: any;
  constructor(formbuelder: FormBuilder) {
    this.loginForm = formbuelder.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
      name: ["", Validators.required]
    });
  }
  private _DoRegister(event) {
    alert(JSON.stringify(this.loginForm.value))
  }
}
