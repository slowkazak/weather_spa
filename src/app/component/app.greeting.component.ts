import {Component, OnInit} from "@angular/core";
import {MainServiceProvider} from "../services/main.service.provider";
@Component({
  moduleId: module.id,
  selector: 'app-greeting',
  templateUrl: '../app.greeting.component.html',
})
export class AppGreetingComponent implements OnInit {
  constructor(private mainserviceprovider: MainServiceProvider) {
  }

  private _pages: any;
  private _editing: boolean = false;
  private _editabletext: string;
  private _editablecuttentid: number;

  private _GetPage(id?: number) {
    this.mainserviceprovider.getPage().subscribe(
      resp => {
        this._pages = resp
      },
      error => console.error(error)
    );
  }

  private _Edit(id: number) {
    this._editabletext = this._pages[id].about;
    this._editablecuttentid = id;

    this._editing = true;
  }

  private _Save() {
    this._pages[this._editablecuttentid].about = this._editabletext;
    this._editabletext = '';
    this._editablecuttentid = 0;
    this._editing = false;
  }

  ngOnInit() {
    this._GetPage()
  }

}
