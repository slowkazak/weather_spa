import {Component,Input,Output,EventEmitter, } from "@angular/core";
import {WeatherInterface} from "../interface/weather.interface"
@Component({
  moduleId: module.id,
  selector:'city-once',
  templateUrl: '../app.city.component.html'
})
export class AppCityComponent{
  @Input()
  public city:WeatherInterface;
  @Input()
  public index:any;
  @Output()
  delete = new EventEmitter();
  constructor(){}

}
