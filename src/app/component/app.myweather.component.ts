import {Component, OnInit} from "@angular/core";
import {MainServiceProvider} from "../services/main.service.provider";
import {AppCityComponent} from "./app.city.component";
import {AppSearchcityComponent} from "./app.searchcity.component";
@Component({
  moduleId: module.id,
  selector: 'app-myweather',
  templateUrl: '../app.myweather.component.html',
  directives: [AppCityComponent, AppSearchcityComponent]
})
export class AppMyweatherComponent implements OnInit {
  private _weatherlist:any;

  constructor(private mainserviceprovider: MainServiceProvider) {
    this._weatherlist = [];
  }

  //delete city from array, rebuild localstorage
  private _delete(ev) {
    this.mainserviceprovider.Remove_data(ev);
    this._weatherlist.splice(ev, 1);
  }
//search city
  private _search(ev) {
    this.getData(ev);
  }
//get city data
  private getData(city?: string) {
    this.mainserviceprovider.getCity(city).subscribe(
      resp => {
        this._weatherlist = this._weatherlist.concat(resp);
      },
      error => console.error(error)
    );
  }
//oninit get city data
  ngOnInit() {
    this.getData();
  }
}
