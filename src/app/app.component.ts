import { Component } from '@angular/core';
import {AppNavComponent} from "./component/app.nav.component"
import { ROUTER_DIRECTIVES } from '@angular/router';
@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  directives: [ROUTER_DIRECTIVES,AppNavComponent]
})
export class AppComponent {
}
