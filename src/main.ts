import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import {APP_ROUTER_PROVIDERS} from './app/routes';

import {HTTP_PROVIDERS,JSONP_PROVIDERS} from '@angular/http';
import { AppComponent, environment } from './app/';
import {MainServiceProvider} from "./app/services/main.service.provider"
if (environment.production) {
  enableProdMode();
}

bootstrap(AppComponent, [
  MainServiceProvider,
  APP_ROUTER_PROVIDERS,JSONP_PROVIDERS ,
  HTTP_PROVIDERS
]);



