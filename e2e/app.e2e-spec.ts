import { SpaWeatherPage } from './app.po';

describe('spa-weather App', function() {
  let page: SpaWeatherPage;

  beforeEach(() => {
    page = new SpaWeatherPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
